﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


[Serializable]
struct GridData
{
    public char[,] SavedGrid;
}
namespace Editor
{
    class Execution
    {
        public bool EndExecution = false;

        public Execution()
        {
        }

    }

    class Object
    {
        public int Xpos, Ypos;
        public int PrevXpos, PrevYpos;
        public Grid grid;

        public virtual bool CheckObstacles(int X, int Y)
        {
            return true;
        }
    }

    class Player : Object
    {
        public Player()
        {
        }

        public Player(Grid _grid)
        {
            grid = _grid;
        }

        public void PlacePlayer(bool Random)
        {         
                Xpos = 0;
                Ypos = 0;
        }

        public override bool CheckObstacles(int X, int Y)
        {
            if (grid.GridArray[X, Y] == 'X')
            {               
                return true;
            }
            return false;
        }

        public bool CheckEnemies(int X, int Y)
        {
            if (grid.GridArray[X, Y] == '+')
            {
                return true;
            }
            else if (grid.GridArray[X, Y] == '#')
            {
                return true;
            }
            return false;
        }
    }       
    
    class Input
    {
        Grid grid;
        Player player;
        Execution execution;

        public Input() { }

        public Input(Grid _grid, Player _player, Execution _execution)
        {
            grid = _grid;
            player = _player;
            execution = _execution;
        }

        public void InputHandle()
        {
            ConsoleKeyInfo keyInfo = Console.ReadKey();
            if (keyInfo.Key == ConsoleKey.R)
            {
                grid.Restart();
            }
            if (keyInfo.Key == ConsoleKey.Escape)
            {

                execution.EndExecution = true;
            }
            if (keyInfo.Key == ConsoleKey.S)
            {
                if (!File.Exists("SavedGrid.binary"))
                {
                    FileStream fileCreate = File.Create("SavedGrid.binary");
                    GridData Data;
                    Data.SavedGrid = grid.GridArray;
                    Data.SavedGrid[player.Xpos, player.Ypos] = '-';
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(fileCreate, Data);
                    fileCreate.Close();
                }
                else
                {
                    FileStream fileCreate = File.OpenWrite("SavedGrid.binary");
                    GridData Data;
                    Data.SavedGrid = grid.GridArray;
                    Data.SavedGrid[player.Xpos, player.Ypos] = '-';
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(fileCreate, Data);
                    fileCreate.Close();                    
                }
                grid.DrawGrid();
            }

            if(keyInfo.Key == ConsoleKey.L)
            {
             grid.LoadGrid();           
            }

            if (keyInfo.Key == ConsoleKey.Spacebar)
            {
                grid.SetObstacle(player.Xpos, player.Ypos);

                bool can = Movement(3);

                if (!can)
                {
                    can = Movement(2);
                }
                if (!can)
                {
                    can = Movement(1);
                }
                if (!can)
                {
                    can = Movement(0);
                }
                if (!can)
                {
                    Console.WriteLine("Im sorry Dave, Im affraid i can't do that");
                }

            }

            if (keyInfo.Key == ConsoleKey.UpArrow)
            {
                Movement(0);
                grid.GridArray[player.PrevXpos, player.PrevYpos] = '-';
            }
            if (keyInfo.Key == ConsoleKey.DownArrow)
            {
                Movement(1);
                grid.GridArray[player.PrevXpos, player.PrevYpos] = '-';
            }
            if (keyInfo.Key == ConsoleKey.LeftArrow)
            {
                Movement(2);
                grid.GridArray[player.PrevXpos, player.PrevYpos] = '-';
            }
            if (keyInfo.Key == ConsoleKey.RightArrow)
            {
                Movement(3);
                grid.GridArray[player.PrevXpos, player.PrevYpos] = '-';
            }
            }

        bool Movement(int direction)
        {
            switch (direction)
            {
                case 0:
                    if (player.Xpos > 0)
                    {
                        if (!player.CheckObstacles(player.Xpos - 1, player.Ypos) && !player.CheckEnemies(player.Xpos - 1, player.Ypos))
                        {
                            //grid.GridArray[player.Xpos, player.Ypos] = '-';
                            grid.GridArray[player.Xpos - 1, player.Ypos] = '*';
                            player.PrevXpos = player.Xpos;
                            player.PrevYpos = player.Ypos;
                            player.Xpos = player.Xpos - 1;
                            return true;
                        }
                    }
                    else
                    {
                        player.PrevXpos = player.Xpos;
                        player.PrevYpos = player.Ypos;
                        return false;
                    }
                    break;

                case 1:
                    if (player.Xpos < (grid.Rows - 1))
                    {
                        if (!player.CheckObstacles(player.Xpos + 1, player.Ypos) && !player.CheckEnemies(player.Xpos + 1, player.Ypos))
                        {
                            //grid.GridArray[player.Xpos, player.Ypos] = '-';
                            grid.GridArray[player.Xpos + 1, player.Ypos] = '*';
                            player.PrevXpos = player.Xpos;
                            player.PrevYpos = player.Ypos;
                            player.Xpos = player.Xpos + 1;
                            return true;
                        }
                    }
                    else
                    {
                        player.PrevXpos = player.Xpos;
                        player.PrevYpos = player.Ypos;
                        return false;
                    }
                    break;

                case 2:
                    if (player.Ypos > 0)
                    {
                        if (!player.CheckObstacles(player.Xpos, player.Ypos - 1) && !player.CheckEnemies(player.Xpos, player.Ypos - 1))
                        {
                            //grid.GridArray[player.Xpos, player.Ypos] = '-';
                            grid.GridArray[player.Xpos, player.Ypos - 1] = '*';
                            player.PrevXpos = player.Xpos;
                            player.PrevYpos = player.Ypos;
                            player.Ypos = player.Ypos - 1;
                            return true;
                        }
                    }
                    else
                    {
                        player.PrevXpos = player.Xpos;
                        player.PrevYpos = player.Ypos;
                        return false;
                    }
                    break;

                case 3:
                    if (player.Ypos < (grid.Columns - 1))
                    {
                        if (!player.CheckObstacles(player.Xpos, player.Ypos + 1) && !player.CheckEnemies(player.Xpos, player.Ypos + 1))
                        {
                            //grid.GridArray[player.Xpos, player.Ypos] = '-';
                            grid.GridArray[player.Xpos, player.Ypos + 1] = '*';
                            player.PrevXpos = player.Xpos;
                            player.PrevYpos = player.Ypos;
                            player.Ypos = player.Ypos + 1;
                            return true;
                        }
                    }
                    else
                    {
                        player.PrevXpos = player.Xpos;
                        player.PrevYpos = player.Ypos;
                        return false;
                    }
                    break;
            }
            return false;
        }
    }

    class Grid
    {
        public int Rows = 15;
        public int Columns = 25;

        public char[,] GridArray;

        private int LineJumpCounter = 0;

        public Player player;

        public Grid()
        {
            GridArray = new char[Rows, Columns];
        }

        public void SetPlayer(Player _player)
        {
            player = _player;
        }

        public void InitGrid()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    GridArray[i, j] = '-';
                }
            }
            
            player.PlacePlayer(false);            

            player.PrevXpos = player.Xpos;
            player.PrevYpos = player.Ypos;
            GridArray[player.Xpos, player.Ypos] = '*';

            //RandomiseLevel(ObstaclesAmount, EnemiesAmount);
        }

        public void DrawGrid()
        {
            Console.Clear();
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    Console.Write(GridArray[i, j]);
                    LineJumpCounter++;
                    if (LineJumpCounter == Columns)
                    {
                        Console.WriteLine();
                        LineJumpCounter = 0;
                    }
                }
            }
            Console.WriteLine("INFO: ");
        }

        public void SetObstacle(int x, int y)
        {          
          GridArray[x, y] = 'X';            
        }

        public void SetEnemies(int x, int y)
        {
            GridArray[x, y] = '+';        
        }

        public void SetPoints(int x, int y)
        {
         GridArray[x, y] = '#';
        }      

        public void Restart()
        {
            InitGrid();
            DrawGrid();
        }        

        public void LoadGrid()
        {
            if (File.Exists("SavedGrid.binary"))
            {
                FileStream fileRead = File.OpenRead("SavedGrid.binary");
                BinaryFormatter formatter = new BinaryFormatter();

                GridData Data = (GridData)formatter.Deserialize(fileRead);
                GridArray = Data.SavedGrid;
                fileRead.Close();
            }
            else
            {
                Console.WriteLine("nope");
            }            
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Execution execution = new Execution();
            Grid grid = new Grid();
            Player player = new Player(grid);
            grid.SetPlayer(player);
            grid.Restart();
            Input input = new Input(grid, player, execution);

            do
            {
                input.InputHandle();

            if ((player.Xpos != player.PrevXpos || player.Ypos != player.PrevYpos))
            {
                grid.DrawGrid();
            }

        } while (!execution.EndExecution);
        }
    }
}
