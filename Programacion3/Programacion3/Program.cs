﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net;

using Newtonsoft.Json.Linq;


[Serializable]
struct GridData
{
    public char[,] SavedGrid;
}

namespace Programacion_3
{
    [Serializable]
    struct Position
    {
        public int x;
        public int y;
    }

    class Execution
    {
        public bool EndExecution = false;
        public bool EndGameRun = false;
        public int prevHighscore;
        public string prevHighscorePlayer;
        string ciudad = "buenosaires";
        public Execution() {
            CheckHighscore();
            Colors();        
        }      
        
        public void CheckHighscore()
        {
            if (File.Exists("HighScore.binary")) {
                FileStream fileScoreRead = File.OpenRead("HighScore.binary");
                BinaryReader br = new BinaryReader(fileScoreRead);

                prevHighscorePlayer = br.ReadString();
                prevHighscore = br.ReadInt32();
                br.Close();
                fileScoreRead.Close();
            }
        }  

        public void Colors()
        {            
            string primeraparte = "https://query.yahooapis.com/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22";
            string segundaparte = "%2C%20tx%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
            string search = primeraparte + ciudad.ToLower() + segundaparte;
            WebRequest req = WebRequest.Create(search);

            try
            {
                WebResponse respuesta = req.GetResponse();
                Stream stream = respuesta.GetResponseStream();
                StreamReader sr = new StreamReader(stream);
                JObject data = JObject.Parse(sr.ReadToEnd());

                string test = (string)data["query"]["results"]["channel"]["item"]["condition"]["text"];
                if (test != null)
                {
                    if (test == "Sunny")
                    {
                        Console.BackgroundColor = ConsoleColor.DarkYellow;
                    }
                    else if (test == "Breezy")
                    {
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                    }
                    else if (test == "Cloudy")
                    {
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.DarkCyan;
                    }
                }
                else
                {
                    Console.WriteLine("ingrese otra cosa");
                }
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    Console.Write("No hay conexion");
                }
                else
                {
                    Console.Write("Error");
                }
            }
        }
    }

    class Input
    {
        Grid grid;
        Player player;
        Player player02; // codigo agregado
        Execution execution;
        Messages messages;

        public Input() { }

        /*/ borrar primer / para opcion 2 (2 players)
        public Input(Grid _grid, Player _player, Player _player02, Execution _execution, Messages _messages)
        {
            grid = _grid;
            player = _player;
            player02 = _player02; // codigo agregado
            execution = _execution;
            messages = _messages;
        }
        /*/ //opcion 2 (1 player)
        public Input(Grid _grid, Player _player, Execution _execution, Messages _messages)
        {
            grid = _grid;
            player = _player;
            execution = _execution;
            messages = _messages;
        }
        //*/
        public void InputHandle()
        {
            ConsoleKeyInfo keyInfo = Console.ReadKey();
            if (keyInfo.Key == ConsoleKey.R)
            {
                grid.Restart();
            }
            if (keyInfo.Key == ConsoleKey.Escape)
            {
                if (!File.Exists("Position.binary"))
                {
                    FileStream fileCreate = File.Create("Position.binary");
                    Position p;
                    p.x = player.Xpos;
                    p.y = player.Ypos;
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(fileCreate, p);
                    fileCreate.Close();
                }
                else
                {
                    FileStream fileCreate = File.OpenWrite("Position.binary");
                    Position p;
                    p.x = player.Xpos;
                    p.y = player.Ypos;
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(fileCreate, p);
                    fileCreate.Close();
                }
                grid.menu.ActiveMenu = true;
                execution.EndGameRun = true;
            }

            if (keyInfo.Key == ConsoleKey.UpArrow)
            {
                if (player.Xpos > 0)
                {
                    if (!player.CheckObstacles(player.Xpos - 1, player.Ypos) && !player.CheckEnemies(player.Xpos - 1, player.Ypos))
                    {
                        grid.GridArray[player.Xpos, player.Ypos] = '-';

                        grid.GridArray[player.Xpos - 1, player.Ypos] = '*';
                        player.PrevXpos = player.Xpos;
                        player.PrevYpos = player.Ypos;
                        player.Xpos = player.Xpos - 1;
                        messages.MessageCounter = 0;
                    }
                }
                else
                {
                    player.PrevXpos = player.Xpos;
                    player.PrevYpos = player.Ypos;
                    messages.BoundsMessage();
                }
            }
            if (keyInfo.Key == ConsoleKey.DownArrow)
            {
                if (player.Xpos < (grid.Rows - 1))
                {
                    if (!player.CheckObstacles(player.Xpos + 1, player.Ypos) && !player.CheckEnemies(player.Xpos + 1, player.Ypos))
                    {
                        grid.GridArray[player.Xpos, player.Ypos] = '-';

                        grid.GridArray[player.Xpos + 1, player.Ypos] = '*';
                        player.PrevXpos = player.Xpos;
                        player.PrevYpos = player.Ypos;
                        player.Xpos = player.Xpos + 1;
                        messages.MessageCounter = 0;
                    }
                }
                else
                {
                    player.PrevXpos = player.Xpos;
                    player.PrevYpos = player.Ypos;
                    messages.BoundsMessage();
                }
            }
            if (keyInfo.Key == ConsoleKey.LeftArrow)
            {
                if (player.Ypos > 0)
                {
                    if (!player.CheckObstacles(player.Xpos, player.Ypos - 1)&& !player.CheckEnemies(player.Xpos, player.Ypos - 1))
                    {
                        grid.GridArray[player.Xpos, player.Ypos] = '-';
                        
                        grid.GridArray[player.Xpos, player.Ypos - 1] = '*';
                        player.PrevXpos = player.Xpos;
                        player.PrevYpos = player.Ypos;
                        player.Ypos = player.Ypos - 1;
                        messages.MessageCounter = 0;
                    }
                }
                else
                {
                    player.PrevXpos = player.Xpos;
                    player.PrevYpos = player.Ypos;
                    messages.BoundsMessage();
                }
            }
            if (keyInfo.Key == ConsoleKey.RightArrow)
            {
                if (player.Ypos < (grid.Columns - 1))
                {
                    if (!player.CheckObstacles(player.Xpos, player.Ypos + 1) && !player.CheckEnemies(player.Xpos, player.Ypos + 1))
                    {
                        grid.GridArray[player.Xpos, player.Ypos] = '-';

                        grid.GridArray[player.Xpos, player.Ypos + 1] = '*';
                        player.PrevXpos = player.Xpos;
                        player.PrevYpos = player.Ypos;
                        player.Ypos = player.Ypos + 1;
                        messages.MessageCounter = 0;
                    }
                }
                else
                {
                    player.PrevXpos = player.Xpos;
                    player.PrevYpos = player.Ypos;
                    messages.BoundsMessage();
                }
            }
            /*/
            //codigo agregado
            if (keyInfo.Key == ConsoleKey.W)
            {
                if (player02.Xpos > 0)
                {
                    if (!player02.CheckObstacles(player02.Xpos - 1, player02.Ypos) && !player02.CheckEnemies(player02.Xpos - 1, player02.Ypos))
                    {
                        grid.GridArray[player02.Xpos, player02.Ypos] = '-';

                        grid.GridArray[player02.Xpos - 1, player02.Ypos] = '*';
                        player02.PrevXpos = player02.Xpos;
                        player02.PrevYpos = player02.Ypos;
                        player02.Xpos = player02.Xpos - 1;
                        messages.MessageCounter = 0;
                    }
                }
                else
                {
                    player02.PrevXpos = player02.Xpos;
                    player02.PrevYpos = player02.Ypos;
                    messages.BoundsMessage();
                }
            }
            if (keyInfo.Key == ConsoleKey.S)
            {
                if (player02.Xpos < (grid.Rows - 1))
                {
                    if (!player02.CheckObstacles(player02.Xpos + 1, player02.Ypos) && !player02.CheckEnemies(player02.Xpos + 1, player02.Ypos))
                    {
                        grid.GridArray[player02.Xpos, player02.Ypos] = '-';

                        grid.GridArray[player02.Xpos + 1, player02.Ypos] = '*';
                        player02.PrevXpos = player02.Xpos;
                        player02.PrevYpos = player02.Ypos;
                        player02.Xpos = player02.Xpos + 1;
                        messages.MessageCounter = 0;
                    }
                }
                else
                {
                    player02.PrevXpos = player02.Xpos;
                    player02.PrevYpos = player02.Ypos;
                    messages.BoundsMessage();
                }
            }
            if (keyInfo.Key == ConsoleKey.A)
            {
                if (player02.Ypos > 0)
                {
                    if (!player02.CheckObstacles(player02.Xpos, player02.Ypos - 1) && !player02.CheckEnemies(player02.Xpos, player02.Ypos - 1))
                    {
                        grid.GridArray[player02.Xpos, player02.Ypos] = '-';

                        grid.GridArray[player02.Xpos, player02.Ypos - 1] = '*';
                        player02.PrevXpos = player02.Xpos;
                        player02.PrevYpos = player02.Ypos;
                        player02.Ypos = player02.Ypos - 1;
                        messages.MessageCounter = 0;
                    }
                }
                else
                {
                    player02.PrevXpos = player02.Xpos;
                    player02.PrevYpos = player02.Ypos;
                    messages.BoundsMessage();
                }
            }
            if (keyInfo.Key == ConsoleKey.D)
            {
                if (player02.Ypos < (grid.Columns - 1))
                {
                    if (!player02.CheckObstacles(player02.Xpos, player02.Ypos + 1) && !player02.CheckEnemies(player02.Xpos, player02.Ypos + 1))
                    {
                        grid.GridArray[player02.Xpos, player02.Ypos] = '-';

                        grid.GridArray[player02.Xpos, player02.Ypos + 1] = '*';
                        player02.PrevXpos = player02.Xpos;
                        player02.PrevYpos = player02.Ypos;
                        player02.Ypos = player02.Ypos + 1;
                        messages.MessageCounter = 0;
                    }
                }
                else
                {
                    player02.PrevXpos = player02.Xpos;
                    player02.PrevYpos = player02.Ypos;
                    messages.BoundsMessage();
                }
            }
            // fin codigo agregado
            //*/
        }
    }

    class Messages
    {
        public int MessageCounter;
        Execution execution;

        public Messages() { }

        public Messages(Execution _execution)
        {
            execution = _execution;
        }

        #region MESSAGES
        public void StartMessage()
        {
            Console.Write("Welcome!\nYou Can Move The '*' Character In The '-' Grid Using The Arrows.\nRemember This:\n-Stay On The Bounds\n-Avoid The 'X' And The Moving '+' To Don't Lose\n-Dont Piss Off The Grid Master\nTo Restart Press 'R'\nTo Exit Just Hit Escape.\nCheerio!");
        }

        public void BoundsMessage()
        {
            switch (MessageCounter)
            {
                case 0:
                    MessageCounter++;
                    Console.WriteLine("You Can´t Move There");
                    break;
                case 1:
                    MessageCounter++;
                    Console.WriteLine("I Told You That You Can´t Move There");
                    break;
                case 2:
                    MessageCounter++;
                    Console.WriteLine("Are You Even Reading?");
                    break;
                case 3:
                    MessageCounter++;
                    Console.WriteLine("This It´s Getting Annoying");
                    break;
                case 4:
                    MessageCounter++;
                    Console.WriteLine("Stop Searching For More Text");
                    break;
                case 5:
                    MessageCounter++;
                    Console.WriteLine("Last Warning...");
                    break;
                case 6:
                    MessageCounter++;
                    Console.WriteLine("bye");
                    execution.EndExecution = true;
                    break;

                default:
                    break;
            }
        }
        #endregion

    }

    class Object
    {
        public int Xpos, Ypos;
        public int PrevXpos, PrevYpos;
        public Grid grid;

        public virtual bool CheckObstacles(int X, int Y)
        {
            return true;
        }
    }

    class Player : Object
    {
        public int points;
        public bool Dead = false; 
        public Player()
        {
        }

        public Player(Grid _grid)
        {
            grid = _grid;            
        }

        public void PlacePlayer(bool Random)
        {
            if (Random)
            {
                int Seed = (int)DateTime.Now.Millisecond;
                Random rndX = new Random(Seed);
                int x = rndX.Next(0, grid.Rows);
                Random rndY = new Random(Seed);
                int y = rndY.Next(0, grid.Columns);
                Xpos = x;
                Ypos = y;
            }
            else
            {
                Xpos = 0;
                Ypos = 0;
            }
        }

        public override bool CheckObstacles(int X, int Y)
        {
            if (grid.GridArray[X, Y] == 'X')
            {
                Dead = true;
                return true;
            }
            return false;
        }

        public bool CheckEnemies(int X, int Y)
        {
            if (grid.GridArray[X, Y] == '+')
            {
                Dead = true;
                return true;
            }
            else if (grid.GridArray[X, Y] == '#')
            {
                points++;                
            }
            return false;
        }
    }

    class Enemy : Object
    {
        public bool PlayerKilled = false;
        bool Up = false;
        bool Left = false;
        public Enemy() { }

        public Enemy(int x, int y, Grid _grid)
        {
            Xpos = x;
            Ypos = y;
            grid = _grid;
        }

        public bool CheckObstacles(int X, int Y,bool Vertical)
        {
            if (Vertical) {
                if (grid.GridArray[X, Y] == 'X' || grid.GridArray[X, Y] == '+')
                {
                    if (Up)
                    {
                        Up = false;
                    }
                    else
                    {
                        Up = true;
                    }
                    return true;
                }
                return false;
            }
            else
            {
                if (grid.GridArray[X, Y] == 'X' || grid.GridArray[X, Y] == '+')
                {
                    if (Left)
                    {
                        Left = false;
                    }
                    else
                    {
                        Left = true;
                    }
                    return true;
                }
                return false;
            }
        }

        public void CheckPlayer(int X, int Y)
        {
            if (grid.GridArray[X, Y] == '*')
            {
                PlayerKilled = true;
            }
        }

        public void VerticalMovement()
        {
            if (Up)
            {
                if (Xpos > 0 && !CheckObstacles(Xpos - 1, Ypos,true))
                {
                    if (grid.GridArray[Xpos, Ypos] != '*')
                    {
                        grid.GridArray[Xpos, Ypos] = '-';
                    }
                    CheckPlayer(Xpos - 1, Ypos);
                    grid.GridArray[Xpos - 1, Ypos] = '+';
                    PrevXpos = Xpos;
                    PrevYpos = Ypos;
                    Xpos = Xpos - 1;
                }
                else
                {
                    PrevXpos = Xpos;
                    PrevYpos = Ypos;
                    Up = false;
                }
            }
            else
            {
                if (Xpos < (grid.Rows - 1) && !CheckObstacles(Xpos + 1, Ypos, true))
                {
                    if (grid.GridArray[Xpos, Ypos] != '*')
                    {
                        grid.GridArray[Xpos, Ypos] = '-';
                    }
                    CheckPlayer(Xpos + 1, Ypos);
                    grid.GridArray[Xpos + 1, Ypos] = '+';
                    PrevXpos = Xpos;
                    PrevYpos = Ypos;
                    Xpos = Xpos + 1;
                }
                else
                {
                    PrevXpos = Xpos;
                    PrevYpos = Ypos;
                    Up = true;
                }
            }
        }

        public void LateralMovement()
        {
            if (Left)
            {
                if (Ypos > 0 && !CheckObstacles(Xpos , Ypos-1, false))
                {
                    if (grid.GridArray[Xpos, Ypos] != '*')
                    {
                        grid.GridArray[Xpos, Ypos] = '-';
                    }
                    CheckPlayer(Xpos , Ypos-1);
                    grid.GridArray[Xpos, Ypos - 1] = '+';
                    PrevXpos = Xpos;
                    PrevYpos = Ypos;
                    Ypos = Ypos - 1;
                }
                else
                {
                    PrevXpos = Xpos;
                    PrevYpos = Ypos;
                    Left = false;
                }
            }
            else
            {
                if (Ypos < (grid.Columns - 1) && !CheckObstacles(Xpos, Ypos +1,false ))
                {
                    if (grid.GridArray[Xpos, Ypos] != '*')
                    {
                        grid.GridArray[Xpos, Ypos] = '-';
                    }
                    CheckPlayer(Xpos, Ypos + 1);
                    grid.GridArray[Xpos, Ypos + 1] = '+';
                    PrevXpos = Xpos;
                    PrevYpos = Ypos;
                    Ypos = Ypos + 1;
                }
                else
                {
                    PrevXpos = Xpos;
                    PrevYpos = Ypos;
                    Left = true;
                }
            }
        }
    }

    class Grid
    {
        public int Rows = 15;
        public int Columns = 25;

        public char[,] GridArray;

        public int ObstaclesAmount = 10;
        public int EnemiesAmount = 5;
        public int PointsAmount = 10;

        private int LineJumpCounter = 0;

        Messages messages;
        public Menu menu;
        public Player player;
        public Player player02; //agregado
        static Random rndX = new Random();
        static Random rndY = new Random((int)DateTime.Now.Ticks);

        public List<Enemy> enemyList = new List<Enemy>();

        public Grid()
        {
            GridArray = new char[Rows, Columns];
        }

        public Grid(Messages _messages, Menu _menu)
        {
            GridArray = new char[Rows, Columns];
            messages = _messages;
            menu = _menu;
        }
        //*/ 1 player
        public void SetPlayer(Player _player, Player _player02)
        {
            player = _player;
        }
        /*/ 2 players
        public void SetPlayer(Player _player, Player _player02)
        {
            player = _player;
            player02 = _player02; // agregado
        }
        //*/
        public void InitGrid(bool LoadLevel)
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    GridArray[i, j] = '-';
                }
            }
            //*/1 player
            if (File.Exists("Position.binary"))
            {
                FileStream fileRead = File.OpenRead("Position.binary");
                BinaryFormatter formatter = new BinaryFormatter();
                Position p = (Position)formatter.Deserialize(fileRead);

                player.Xpos = p.x;
                player.Ypos = p.y;

                fileRead.Close();
            }
            else {
                player.PlacePlayer(true);
            }

            player.PrevXpos = player.Xpos;
            player.PrevYpos = player.Ypos;
            GridArray[player.Xpos, player.Ypos] = '*';
            /*/2 Players
            player.PlacePlayer(true);
            player02.PlacePlayer(true); //agregado
     
            // no se lo que hace pero lo copio
            player02.PrevXpos = player02.Xpos;
            player02.PrevYpos = player02.Ypos;
            GridArray[player02.Xpos, player02.Ypos] = '*';
            // fin agregado
            //*/


                RandomiseLevel(ObstaclesAmount, EnemiesAmount,!LoadLevel);
            
        }

        public void DrawGrid()
        {
            Console.Clear();
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    Console.Write(GridArray[i, j]);
                    LineJumpCounter++;
                    if (LineJumpCounter == Columns)
                    {
                        Console.WriteLine();
                        LineJumpCounter = 0;
                    }
                }
            }
            Console.WriteLine("SCORE: " + player.points);
        }

        public int SetObstacle(int x, int y, int rep)
        {
            if (GridArray[x, y] != '*' && GridArray[x, y] != 'X')
            {
                GridArray[x, y] = 'X';
                rep++;
            }
            return rep;
        }

        public int SetEnemies(int x, int y, int rep)
        {
            if (GridArray[x, y] != '*' && GridArray[x, y] != 'X' && GridArray[x, y] != '+')
            {
                GridArray[x, y] = '+';
                Enemy newEnemy = new Enemy(x, y, this);
                enemyList.Add(newEnemy);
                rep++;
            }
            return rep;
        }

        public int SetPoints(int x, int y, int rep)
        {
            if (GridArray[x, y] != '*' && GridArray[x, y] != 'X' && GridArray[x, y] != '+' && GridArray[x, y] != '#')
            {
                GridArray[x, y] = '#';
                rep++;
            }
            return rep;
        }

        public void RandomiseLevel(int ObstaclesAmount, int enemiesAmount, bool RandomizeObstacles)
        {
            int rep = 0;
            int x, y;
            if (RandomizeObstacles) {
                for (int i = 0; i <= ObstaclesAmount; i++)
                {
                    x = rndX.Next(0, Rows);
                    y = rndY.Next(0, Columns);
                    rep = SetObstacle(x, y, rep);
                    i = rep;
                }
                rep = 0;
            }
            else
            {
                LoadGrid();
            }

            for (int i = 0; i <= enemiesAmount; i++)
            {
                x = rndX.Next(0, Rows);
                y = rndY.Next(0, Columns);
                rep = SetEnemies(x, y, rep);
                i = rep;
            }
            rep = 0;
            for (int i = 0; i <= PointsAmount; i++)
            {
                x = rndX.Next(0, Rows);             
                y = rndY.Next(0, Columns);
                rep = SetPoints(x, y, rep);
                i = rep;
            }
        }

        public void Restart()
        {
            player.points = 0;
            enemyList.Clear();
            InitGrid(false);
            DrawGrid();
            messages.StartMessage();
            player.Dead = false;                                   
            //player02.Dead = false;
        }

        public void EnemyMovement(bool VerticalMovement)
        {
            for (int i = 0; i < enemyList.Count; i++)
            {
                if (VerticalMovement)
                {
                    enemyList[i].VerticalMovement();
                }
                else
                {
                    enemyList[i].LateralMovement();
                }
                if (enemyList[i].PlayerKilled)
                {
                    player.Dead = true;
                    break;
                }
            }            
        }

        public void LoadGrid()
        {
            if (File.Exists("../../../Editor/bin/Debug/SavedGrid.binary"))
            {
                FileStream fileRead = File.OpenRead("../../../Editor/bin/Debug/SavedGrid.binary");
                BinaryFormatter formatter = new BinaryFormatter();

                GridData Data = (GridData)formatter.Deserialize(fileRead);
                GridArray = Data.SavedGrid;
                fileRead.Close();
            }
            else
            {
                Console.WriteLine("There's not a saved level so there you have a random level");
                Console.ReadKey();
            }
        }
    }

    class Menu
    {
        public bool ActiveMenu;
        public bool ActivePlayerSelection = false;
        bool ActiveWellcomeMessage = true;

        int pos = 0;
        Execution execution;

        public Menu() { }

        public Menu(Execution _execution)
        {
            execution = _execution;
        }

        public void ShowMenu()
        {
            Console.Clear();
            if (pos == 0)
            {
                Console.WriteLine("> -Play");
                Console.WriteLine("  -Exit");
            }
            else
            {
                Console.WriteLine("  -Play");
                Console.WriteLine("> -Exit");
            }
            Console.WriteLine("\n\nLast Highscore: "+execution.prevHighscore+" by "+execution.prevHighscorePlayer);
        }

        public void MenuInput()
        {
            ConsoleKeyInfo keyInfo = Console.ReadKey();
            if (keyInfo.Key == ConsoleKey.UpArrow)
            {
                if (pos == 0)
                {
                    pos = 1;
                }
                else
                {
                    pos = 0;
                }
                ShowMenu();
            }
            if (keyInfo.Key == ConsoleKey.DownArrow)
            {
                if (pos == 0)
                {
                    pos = 1;
                }
                else
                {
                    pos = 0;
                }
                ShowMenu();
            }
            if (keyInfo.Key == ConsoleKey.Enter)
            {
                if (pos == 0)
                {
                    ActiveMenu = false;

                }
                else
                {
                    execution.EndExecution = true;
                }
            }
        }

        public void SetWelcomeText()
        {
            string WellcomeMessage;
            if (ActiveWellcomeMessage) {
                if (!File.Exists("Wellcome Message.txt"))
                {
                    FileStream fileWrite = File.Create("Wellcome Message.txt");
                    StreamWriter sw = new StreamWriter(fileWrite);
                    Console.WriteLine("Write your Welcome Message:");
                    WellcomeMessage = Console.ReadLine();
                    sw.WriteLine(WellcomeMessage);
                    sw.Close();
                    fileWrite.Close();
                    Console.WriteLine("Cool... Now press any key to continue.");
                    Console.ReadKey();
                }
                else
                {
                    FileStream fileRead = File.OpenRead("Wellcome Message.txt");
                    StreamReader sr = new StreamReader(fileRead);
                    WellcomeMessage = sr.ReadLine();
                    sr.Close();
                    fileRead.Close();
                    Console.WriteLine(WellcomeMessage);
                    Console.WriteLine("\n\nPress any key to continue.");
                    Console.ReadKey();
                }
                ActiveWellcomeMessage = false;
            }
        }

    }

    class Game
    {
        Execution execution;
        Messages messages;
        Grid grid;
        Player player;
        Player player02;
        Input input;
        Menu menu;


        public Game() {
            execution = new Execution();
            messages = new Messages(execution);
            menu = new Menu(execution);
            menu.ActiveMenu = true;
            grid = new Grid(messages, menu);
            player = new Player(grid);
            player02 = new Player(grid);
            grid.SetPlayer(player, player02);
            /*/ 2 players
            input = new Input(grid, player, player02, execution, messages);
            /*/ // 1 player
            input = new Input(grid, player, execution, messages);
            //*/

        }

        public void RunGame() {            
            do
            {
                if (menu.ActiveMenu)
                {
                    menu.SetWelcomeText();
                    menu.ShowMenu();
                    menu.MenuInput();                    
                }
                else
                {
                    grid.Restart();
                    execution.EndGameRun = false;
                    do                    {
                        grid.EnemyMovement(false);
                        if (Console.KeyAvailable)
                        {
                            input.InputHandle();
                        }
                        if (player.Dead || player02.Dead)
                        {
                            menu.ActiveMenu = true;
                            if (!File.Exists("HighScore.binary"))
                            {
                                FileStream fileScoreCreate = File.Create("HighScore.binary");
                                BinaryWriter bw = new BinaryWriter(fileScoreCreate);
                                Console.WriteLine("You Lose Boi");
                                Console.Write("Write your name: ");
                                string Name = Console.ReadLine();
                                bw.Write(Name);
                                bw.Write(player.points);
                                bw.Close();
                                fileScoreCreate.Close();
                            }
                            else
                            {                             
                                Console.WriteLine("You Lose Boi");
                                if (execution.prevHighscore >= player.points)
                                {
                                    Console.WriteLine("Last highscore was of " + execution.prevHighscore + " by " + execution.prevHighscorePlayer);
                                }
                                else
                                {
                                    FileStream fileScoreWrite = File.OpenWrite("HighScore.binary");
                                    BinaryWriter bw = new BinaryWriter(fileScoreWrite);
                                    Console.Write("Write your name: ");
                                    string NewName = Console.ReadLine();
                                    bw.Write(NewName);
                                    bw.Write(player.points);
                                    bw.Close();
                                    fileScoreWrite.Close();
                                }                             
                            }

                            Console.WriteLine("Hit Any Key To Restart");
                            Console.ReadKey();
                            execution.CheckHighscore();
                            break;

                        }else if ((player.Xpos != player.PrevXpos || player.Ypos != player.PrevYpos) || (player02.Xpos != player02.PrevXpos || player02.Ypos != player02.PrevYpos))
                        {
                            grid.DrawGrid();
                        }
                        Thread.Sleep(500);

                    } while (!execution.EndGameRun);
                }
            } while (!execution.EndExecution);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();
            game.RunGame();
        }
    }
}